import { server } from '../../utils';
import React from 'react';
import "./Styling/style.scss";
import CircularProgress from './CircularProgress';





// export class Register extends Component {
  const Register = () => {

  // constructor(props) {
  //   super(props);
  
  // };
  const [formData, setFormData] = React.useState({
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  })

  const [submitting, setSubmitting] = React.useState(false)

  const handleSubmit = async e => {
    e.preventDefault()
    const { firstName, lastName, email, password } = formData
    const { success, data } = await server.postAsync('/auth/register', {
      firstName,
      lastName,
      email,
      password
    })
    if (success) {
      window.location.replace(data)
      return
    }
  }

  
    return (
      <div className="base-container">
        <div className="header">Register</div>
        <form method="post" noValidate onSubmit={handleSubmit}>
        <div className="content">
          <div className="image">
            <img src={'login.png'} alt="loginImg" />
          </div>
          
            <div className="form">
            <div className="form-group" required fullwidth="true">
              <label htmlFor="firstName">First Name</label>
              <input type="text" name="firstName" placeholder="First Name" 
                defaultValue={formData.firstName} 
                onChange={e => setFormData({ ...formData, firstName: e.target.value })} />
            </div>
            <div className="form-group" required fullwidth="true"> 
              <label htmlFor="lastName">Last Name</label>
              <input type="text" name="lastName" placeholder="Last Name"
                defaultValue={formData.lastName}
                onChange={e => setFormData({ ...formData, lastName: e.target.value })} />
            </div>
            <div className="form-group" required fullwidth="true">
              <label htmlFor="email">Email</label>
              <input type="email" name="email" placeholder="Email" 
                defaultValue={formData.email}
                onChange={e => setFormData({ ...formData, email: e.target.value })} />
            </div>
            <div className="form-group" required fullwidth="true">
              <label htmlFor="password">Password</label>
              <input type="password" name="password" placeholder="Password" 
                defaultValue={formData.password}
                onChange={e => setFormData({ ...formData, password: e.target.value })} />
            </div>
            </div>     
            
        </div>
        <div className="footer">
          <button type="submit" className="btn" disabled={submitting}>
          {submitting && (
              <CircularProgress/>  
              )}
              {submitting ? 'Registering...' : 'Register'}
          </button>
        </div>
        </form>
      </div>
    )
  }


export default Register
