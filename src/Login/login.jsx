import { server } from '../../utils';
import React from 'react'
import "./Styling/style.scss";
import CircularProgress from './CircularProgress';



// import {
//   BrowserRouter,
//   Route,
//   Link
// } from "react-router-dom";
// import MainDashboard from '../../DashboardComponent/Main/MainDashboard';


// export class Login extends Component {
  const Login = () => {
    const [formData, setFormData] = React.useState({ email: '', password: '' })
    const [submitting, setSubmitting] = React.useState(false)

    const handleSubmit = async e => {
      e.preventDefault();
      const { email, password } = formData
      const { success, data } = await server.postAsync('/auth/login', {
        email,
        password
      })
      
      if (success) {
        window.location.replace(data)
        return 
        }
      
      }
  
    return (
      <div className="base-container">
        <div className="header">Login</div>
        <form method="post" noValidate onSubmit={handleSubmit}>
        <div className="content">
          <div className="image">
            <img src={'login.png'} />
          </div>
          <div className="form">
            <div className="form-group" required fullwidth="true">
              <label htmlFor="email">Email</label>
              <input type="text" name="email" placeholder="Email"
              defaultValue={formData.email}
              onChange={e => setFormData({ ...formData, email: e.target.value })} />
            </div>
            <div className="form-group" required fullwidth="true">
              <label htmlFor="password">Password</label>
              <input type="password" name="password" placeholder="Password"
              defaultValue={formData.password}
              onChange={e => setFormData({ ...formData, password: e.target.value })} />
            </div>
          </div>         
        </div>
        <div className="footer">
            <button disabled={submitting} type="submit" className="btn" fullwidth="true">
            {submitting && (
               <CircularProgress/>
              )}
              {submitting ? 'Signing in...' : 'Sign In'}
            </button>
          <a href={`${process.env.BASE_API_URL}/auth/google`}>
            <button type="button" className="btn-google">
              <div className="image-google">
                <img src={'google-icon.svg'} />
              </div>
              Sign in with Google
            </button>
          </a>
        </div>
        </form>
      </div>
    )
  
}

export default Login
