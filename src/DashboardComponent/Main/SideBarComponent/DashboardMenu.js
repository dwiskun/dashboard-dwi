import React, { Component } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import '../../Styling/dashboardComponent.css';


// import React from 'react'

function DashboardMenu() {
  const router = useRouter()
  return (
    <div className="side-menu">
      <img className="dashboard-icon" src={'dashboardIcon.svg'}/>
        <div className="menu-dashboard">
          <span onClick={() => router.push('/content')}>
            <p >Dashboard</p> 
          </span>
        </div>
        <img className="dictation-icon" src={'mic.svg'} alt='dictationIcon'/>
          <div className="menu-dictation">
          <span onClick={() => router.push('/Dictation')}>
            <p>Dictation</p>
          </span>
          </div>
        <img className="user-icon" src={'users.svg'} alt='userIcon'/>
          <div className="menu-users">
          <span onClick={() => router.push('/User')}>
            <p>Users</p>
          </span>
          </div>
        <img className="appointment-icon" src={'calendar.svg'} alt='appointmentIcon'/>
          <div className="menu-appointment">
          <span onClick={() => router.push('/Appointment')}>
            <p>Appointment</p>
          </span>
          </div>
        <img className="symptoms-icon" src={'user-x.svg'} alt='symptomsIcon'/>
          <div className="menu-symptoms">
          <span onClick={() => router.push('/Symptoms')}>
            <p>Symptoms</p>
          </span>
          </div>
        <img className="alert-icon" src={'alert-triangle.svg'} alt='alertIcon'/>
          <div className="menu-alerts">
          <span onClick={() => router.push('/Alert')}>
            <p>Alerts</p>
          </span>
          </div>        
      </div>
  );
}

// function DashboardMenu({router}) {
 
//   return (
//     <div className="side-menu" selctedKeys={[router]}>
//       <img className="dashboard-icon" src={'dashboardIcon.svg'}/>
//         <div className="menu-dashboard" key={"/content"}>
//           <Link href={"/content"}>
//             <p >Dashboard</p> 
//           </Link>
//         </div>
//         <img className="dictation-icon" src={'mic.svg'} alt='dictationIcon'/>
//           <div className="menu-dictation" key={"/Dictation"}>
//           <Link href={"/Dictation"}>
//             <p>Dictation</p>
//           </Link>
//           </div>
//         <img className="user-icon" src={'users.svg'} alt='userIcon'/>
//           <div className="menu-users" key={"/User"}>
//           <Link href={"/User"}>
//             <p>Users</p>
//           </Link>
//           </div>
//         <img className="appointment-icon" src={'calendar.svg'} alt='appointmentIcon'/>
//           <div className="menu-appointment" key={"/Appointment"}>
//             <Link href={"/Appointment"}>
//             <p>Appointment</p>
//             </Link>
//           </div>
//         <img className="symptoms-icon" src={'user-x.svg'} alt='symptomsIcon'/>
//           <div className="menu-symptoms" key={"/Symptoms"}>
//             <Link href={"/Symptoms"}>
//             <p>Symptoms</p>
//             </Link>
//           </div>
//         <img className="alert-icon" src={'alert-triangle.svg'} alt='alertIcon'/>
//           <div className="menu-alerts" key={"/Alert"}>
//             <Link href={"/Alert"}>
//             <p>Alerts</p>
//             </Link>
//           </div>        
//       </div>
//   );
// }



export default DashboardMenu
