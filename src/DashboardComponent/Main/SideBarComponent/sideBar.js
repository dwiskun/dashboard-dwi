import React, { Component } from 'react'
import '../../Styling/dashboardComponent.css';
import DashboardMenu from './DashboardMenu';
export class sideBar extends Component {
  render() {
    return (
      <div className="side-bar">
        <div className="core-modules">
          <p>Core Modules</p>
        </div>
        <DashboardMenu />
      </div>
    )
  }
}

export default sideBar
