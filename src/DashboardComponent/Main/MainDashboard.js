import React, { Component } from 'react'
import TopBar from './topBar';
import SideBar from './SideBarComponent/sideBar';
import NavBar from './navBar';
import '../Styling/dashboardComponent.css';

export class MainDashboard extends Component {
  render() {
    return (
      <div className="container">
      <TopBar />
      <NavBar />
      <SideBar />
    </div>
    )
  }
}

export default MainDashboard;
