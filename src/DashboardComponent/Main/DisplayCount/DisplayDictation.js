import React, { Component } from 'react'
// import gelombang from '../../Styling/Vector3.png'


export class DisplayDictation extends Component {
  render() {
    return (
      <div>
        <div className="display-dict">
        <div className="txt-app">
          <p>DICTATION</p>
        </div>
        <div className="txt-app-count">
          <p>2510</p>
        </div>
        <div className="txt-app-day">
          <p>120 <span>Today</span></p>
        </div>
          <img className="app-vector" src={'Vector3.png'} alt='gelombang'/>
        </div>
      </div>
    )
  }
}

export default DisplayDictation
