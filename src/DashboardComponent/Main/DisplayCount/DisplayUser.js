import React, { Component } from 'react'
// import gelombang from '../../Styling/Vector2.png'

class DisplayUser extends Component {
  render() {
    return (
      <div>
        <div className="display-user">
        <div className="txt-user">
            <p>NEW USER</p>
          </div>
          <div className="txt-user-count">
            <p>10012</p>
          </div>
          <div className="txt-user-day">
            <p>28 <span>Today</span></p>
          </div>
          <img className="user-vector" src={'Vector2.png'} alt='gelombang'/>
         </div>
      </div>
    )
  }
}

export default DisplayUser;
