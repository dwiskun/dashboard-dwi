import React, { Component } from 'react'
// import gelombang from '../../Styling/Vector3.png'

export class DisplayAppointment extends Component {
  render() {
    return (
      <div>
      <div className="display-app">
      <div className="txt-app">
          <p>APPOINTMENTS</p>
        </div>
        <div className="txt-app-count">
          <p>112</p>
        </div>
        <div className="txt-app-day">
          <p>30 <span>Today</span></p>
        </div>
        <img className="app-vector" src={'Vector3.png'} alt='gelombang'/>
       </div>
    </div>
    )
  }
}

export default DisplayAppointment
