import passport from 'passport'
import passportGoogle from 'passport-google-oauth'
import { to } from 'await-to-js'

import { getUserByProviderId, createUser } from '../../database/user'
import { signToken, getRedirectUrl } from '../utils'
import { ROLES } from '../../../utils'

const GoogleStrategy = passportGoogle.OAuth2Strategy

const strategy = app => {
  const strategyOptions = {
    clientID: `434456750990-4dddbgi91ssaoh92eosqkaubhghotumd.apps.googleusercontent.com`,
    clientSecret: `8Z1lKSXD7NAL2tNpZ9QLWerV`,
    callbackURL: `https://dashboard-dwi.vercel.app/api/auth/google/callback`
  }

const verifyCallback = async (
    accessToken,
    refreshToken,
    profile,
    done
  ) => {
    let [err, user] = await to(getUserByProviderId(profile.id))
    if (err || user) {
      return done(err, user)
    }

    const verifiedEmail = profile.emails.find(email => email.verified) || profile.emails[0]

    const [createdError, createdUser] = await to(
      createUser({
        provider: profile.provider,
        providerId: profile.id,
        firstName: profile.name.givenName,
        lastName: profile.name.familyName,
        displayName: profile.displayName,
        email: verifiedEmail.value,
        password: null,
        role: ROLES.Customer
      })
    )

    return done(createdError, createdUser)
  }

  passport.use(new GoogleStrategy(strategyOptions, verifyCallback))
    app.get(
        `${process.env.BASE_API_URL}/auth/google`,
        passport.authenticate('google', {
          scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
          ]
        })
      )
    
      app.get(
        `${process.env.BASE_API_URL}/auth/google/callback`,
        passport.authenticate('google', { failureRedirect: '/' }),
        (req, res) => {
          return res
            .status(200)
            .cookie('jwt', signToken(req.user), {
              httpOnly: true
            })
            .redirect(getRedirectUrl(req.user.role))
        }
      )
  return app
}

export { strategy }