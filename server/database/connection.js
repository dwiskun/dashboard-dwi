import { connect, connection } from 'mongoose'

const connectToDatabase = async () =>
  await connect('mongodb+srv://dwiskun2:Namasayadwi2@cluster0-a3zhw.mongodb.net/dashboard?retryWrites=true&w=majority', {
    useFindAndModify: false,
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    useNewUrlParser: true
  })

export { connectToDatabase, connection }