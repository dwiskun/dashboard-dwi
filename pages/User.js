import React, { Component } from 'react'
import '../src/DashboardComponent/Styling/dashboardComponent.css';
import MainDashboard from '../src/DashboardComponent/Main/MainDashboard';
export class User extends Component {
  render() {
    return (
      <div>
        <MainDashboard />
        <div className="user">
          <h1>Users</h1>
        </div>
      </div>
    )
  }
}

export default User
