import React, { Component } from 'react'
import MainDashboard from '../src/DashboardComponent/Main/MainDashboard'

export class Alert extends Component {
  render() {
    return (
      <div>
        <MainDashboard/>
        <div className="alert">
          <h1>Alerts</h1>
        </div>
      </div>
    )
  }
}

export default Alert
