



import React, { Component } from 'react'
import '../src/DashboardComponent/Styling/dashboardComponent.css';
import DisplayUser from '../src/DashboardComponent/Main/DisplayCount/DisplayUser';
import DisplayAppointment from '../src/DashboardComponent/Main/DisplayCount/DisplayAppointment';
import DisplayAlert from '../src/DashboardComponent/Main/DisplayCount/DisplayAlert';
import DisplayDictation from '../src/DashboardComponent/Main/DisplayCount/DisplayDictation';
import TableContent from '../src/DashboardComponent/Main/DisplayCount/TableContent';
import UserContent from '../src/DashboardComponent/Main/DisplayCount/UserContent';
import Filter from '../src/DashboardComponent/Main/DisplayCount/filter';
import Page from '../src/DashboardComponent/Main/Page';
import MainDashboard from '../src/DashboardComponent/Main/MainDashboard';
export class Content extends Component {
  render() {
    return (
      <div>
        <MainDashboard/>
        <DisplayUser/>
        <DisplayAppointment/>       
        <DisplayAlert/>       
        <DisplayDictation/>
        <TableContent />
        <UserContent />
        <Filter/>
        <Page/>
      </div>
    )
  }
}

export default Content
