import React, { Component } from 'react'
import '../src/DashboardComponent/Styling/dashboardComponent.css';
import MainDashboard from '../src/DashboardComponent/Main/MainDashboard';
export class Dictation extends Component {
  render() {
    return (
      <div>
        <MainDashboard />
        <div className="dictation">
          <h1>Dictations</h1>
        </div>
      </div>
    )
  }
}

export default Dictation
    