import React, { Component } from 'react'
import MainDashboard from '../src/DashboardComponent/Main/MainDashboard'
import '../src/DashboardComponent/Styling/dashboardComponent.css';

export class Symptoms extends Component {
  render() {
    return (
      <div>
        <MainDashboard />
        <div className="symptoms">
          <h1>Symptoms</h1>
        </div>
      </div>
    )
  }
}

export default Symptoms
