const withSass = require('@zeit/next-sass');
const withCSS = require("@zeit/next-css");
const { parsed: localEnv } = require('dotenv').config();

module.exports = 
  withCSS(withSass({
  webpack (config, options) {
      config.module.rules.push({
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: 'file-loader',
        options: {
          outputPath: 'images',
        },
      });
      return config;
    },
    env: {
      BASE_API_URL: `/api`
    }
  }));




